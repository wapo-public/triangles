const trianglify = require('trianglify');
const fs = require('fs');
const fsPath = require('path');

const _width = +getParams().width || 1920;
const _height = +getParams().height || 1080;
const _path = +getParams().path || './triangles';
const _cellSize = +getParams().cellSize || 75;
const _variance = +getParams().variance || 0.75;

let count = +getParams().count || +getParams().n || 1;

CreateTrianglify(1);

function CreateTrianglify(n) {
  if (!(n <= count)) return;
  const canvas = trianglify({
    width: _width,
    height: _height,
    cellSize: _cellSize,
    variance: _variance,
    fill: true,
  }).toCanvas();

  let fileName = n.toFixed(0) + '.png';
  console.log(fsPath.join(_path, fileName));
  fs.mkdir(_path, { recursive: true }, () => {
    const file = fs.createWriteStream(fsPath.join(_path, fileName), { autoClose: true });
    let f = canvas.createPNGStream().pipe(file);

    f.on('close', () => {
      CreateTrianglify(++n);
    });
  });
}

function getParams() {
  const args = {};
  process.argv.slice(2, process.argv.length).forEach((arg) => {
    if (arg.slice(0, 2) === '--') {
      const longArg = arg.split('=');
      const longArgFlag = longArg[0].slice(2, longArg[0].length);
      const longArgValue = longArg.length > 1 ? longArg[1] : true;
      args[longArgFlag] = longArgValue;
    } else if (arg[0] === '-') {
      const flags = arg.slice(1, arg.length).split('');
      flags.forEach((flag) => {
        args[flag] = true;
      });
    }
  });
  return args;
}
